package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

func init() {
	CmdMain.AddCommand(CmdDebug, CmdAll, CmdCheckout, CmdCi, CmdClone, CmdMergeRequests, CmdOpen, CmdMy, CmdServer)
	CmdAll.AddCommand(CmdAllProjects)
	CmdCi.AddCommand(CmdCiArtifacts, CmdCiDownload, CmdCiJobs, CmdCiPipelines)
	CmdMy.AddCommand(CmdMyMergeRequests, CmdMyProjects)
	CmdServer.AddCommand(CmdServerAdd, CmdServerRemove, CmdServerSetUrl, CmdServerSetToken)
	CmdServerSetToken.AddCommand(CmdServerSetTokenNetrc)

	var configFile string
	home, _ := os.UserHomeDir()
	if home != "" {
		configFile = filepath.Join(home, ".gitlab-cli.toml")
	}

	for _, c := range canFormat {
		c.Flags().Var(&Flags.Format, "format", "format output")
	}

	CmdMain.PersistentFlags().BoolVar(&Flags.CI, "ci", false, "use GitLab CI environment variables for everything")
	CmdMain.PersistentFlags().StringVarP(&Flags.ConfigFile, "config-file", "c", configFile, "path to the configuration file")
	CmdMain.PersistentFlags().StringVarP(&Flags.Server, "server", "s", "", "name of the GitLab server to query")

	CmdMain.PersistentFlags().BoolVar(&Flags.Debug, "debug", false, "print debug info")
	CmdMain.PersistentFlags().MarkHidden("debug")

	CmdAllProjects.Flags().BoolVarP(&Flags.Verbose, "verbose", "v", false, "include URLs")

	CmdCiArtifacts.Flags().StringVarP(&Flags.Project, "project", "p", "", "full path of the project to query")
	CmdCiArtifacts.Flags().StringVarP(&Flags.Ref, "ref", "r", "", "repository ref (branch, tag, etc) to query")
	CmdCiArtifacts.Flags().StringVarP(&Flags.Pipeline, "pipeline", "l", "", "ID of the pipeline to query, or 'latest'")

	CmdCiDownload.Flags().StringVarP(&Flags.Project, "project", "p", "", "full path of the project to query")
	CmdCiDownload.Flags().StringVarP(&Flags.Ref, "ref", "r", "", "repository ref (branch, tag, etc) to query")
	CmdCiDownload.Flags().StringVarP(&Flags.Pipeline, "pipeline", "l", "", "ID of the pipeline to query, or 'latest'")
	CmdCiDownload.Flags().StringVarP(&Flags.Output, "output", "o", "", "output path")

	CmdCiJobs.Flags().StringVarP(&Flags.Project, "project", "p", "", "full path of the project to query")
	CmdCiJobs.Flags().StringVarP(&Flags.Ref, "ref", "r", "", "repository ref (branch, tag, etc) to query")
	CmdCiJobs.Flags().StringVarP(&Flags.Pipeline, "pipeline", "l", "", "ID of the pipeline to query, or 'latest'")

	CmdCiPipelines.Flags().StringVarP(&Flags.Project, "project", "p", "", "full path of the project to query")
	CmdCiPipelines.Flags().StringVarP(&Flags.Ref, "ref", "r", "", "repository ref (branch, tag, etc) to query")

	CmdMergeRequests.Flags().BoolVar(&Flags.Authored, "authored", false, "show merge requests authored by me")
	CmdMergeRequests.Flags().BoolVar(&Flags.Assigned, "assigned", false, "show merge requests assigned to me")
	CmdMergeRequests.Flags().VarP((*MergeRequestState)(&Flags.State), "state", "t", "only show open/closed/etc merge requests\naccepted values are: all, open[ed] (default), closed, locked, or merged")

	CmdMyProjects.Flags().BoolVarP(&Flags.Verbose, "verbose", "v", false, "include URLs")

	CmdMyMergeRequests.Flags().BoolVarP(&Flags.All, "all", "a", false, "show all merge requests\nequivalent to --state all")
	CmdMyMergeRequests.Flags().VarP((*MergeRequestState)(&Flags.State), "state", "t", "only show open/closed/etc merge requests\naccepted values are: all, open[ed] (default), closed, locked, or merged")
}

func args(fns ...cobra.PositionalArgs) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		for _, fn := range fns {
			if err := fn(cmd, args); err != nil {
				return err
			}
		}
		return nil
	}
}

func unindent(s string) string {
	re := regexp.MustCompile(`\S`)
	n := -1
	l := strings.Split(s, "\n")
	for _, l := range l {
		if !re.MatchString(l) {
			continue
		}

		m := 0
		for _, c := range l {
			if c == '\t' {
				m++
			} else {
				break
			}
		}

		if n < 0 || m < n {
			n = m
		}
	}

	if n <= 0 {
		return s
	}

	for i := range l {
		if len(l[i]) < n {
			continue
		}
		l[i] = l[i][n:]
	}
	return strings.Join(l, "\n")
}

func ArgVarAt(pos int, v pflag.Value) cobra.PositionalArgs {
	return func(cmd *cobra.Command, args []string) error {
		if len(args) <= pos {
			return nil
		}

		return v.Set(args[pos])
	}
}

type FormatSpecifier int

const (
	FormatHuman FormatSpecifier = iota
	FormatColumn
	FormatJSON
)

func (_ *FormatSpecifier) Type() string { return "format" }

func (s *FormatSpecifier) String() string {
	switch *s {
	case FormatHuman:
		return "human"
	case FormatColumn:
		return "column"
	case FormatJSON:
		return "JSON"
	default:
		return fmt.Sprintf("unknown format (%d)", *s)
	}
}

func (s *FormatSpecifier) Set(v string) error {
	switch strings.ToLower(v) {
	case "", "human":
		*s = FormatHuman
	case "col", "column":
		*s = FormatColumn
	case "json":
		*s = FormatJSON
	default:
		return fmt.Errorf("invalid format: %q", v)
	}
	return nil
}

type MergeRequestState string

func (_ *MergeRequestState) Type() string   { return "state" }
func (s *MergeRequestState) String() string { return string(*s) }

func (s *MergeRequestState) Set(v string) error {
	switch v {
	case "open", "opened":
		*s = "opened"
	case "all", "closed", "locked", "merged":
		*s = MergeRequestState(v)
	default:
		return fmt.Errorf("invalid merge request state: %q", v)
	}
	return nil
}

type IntVar int64

func (_ *IntVar) Type() string   { return "int" }
func (v *IntVar) String() string { return strconv.FormatInt(int64(*v), 10) }

func (v *IntVar) Set(s string) error {
	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		return err
	}

	*v = IntVar(i)
	return nil
}

var Flags struct {
	ConfigFile, Server string
	CI, Verbose, All   bool
	Format             FormatSpecifier
	Output             string

	Debug bool

	Project, Ref string
	Pipeline     string

	ID                 int64
	State              string
	Authored, Assigned bool
}

var canFormat = []*cobra.Command{
	CmdCiArtifacts,
	CmdCiJobs,
	CmdCiPipelines,
}

var CmdMain = &cobra.Command{
	Use:   "gl",
	Short: "A simple command line interface for GitLab",

	PersistentPreRun: preMain,
}

var CmdDebug = &cobra.Command{
	Use:    "debug",
	Hidden: true,
}

var CmdAll = &cobra.Command{
	Use:   "all",
	Short: "List all things",
}

var CmdAllProjects = &cobra.Command{
	Use:   "projects [<search_string>]",
	Short: "List all projects",
	Args:  cobra.MaximumNArgs(1),
	Run:   AllProjects,
}

var CmdCheckout = &cobra.Command{
	Use:   "checkout <merge_request_id>",
	Short: "Checkout a merge request",
	Args:  args(cobra.ExactArgs(1), ArgVarAt(0, (*IntVar)(&Flags.ID))),
	Run:   Checkout,
}

var CmdCi = &cobra.Command{
	Use:   "ci",
	Short: "Commands related to GitLab CI",
}

var CmdCiArtifacts = &cobra.Command{
	Use:   "artifacts <job_id>|<job_name>",
	Short: "Inspect CI artifacts",
	Args:  cobra.ExactArgs(1),
	Run:   CiArtifacts,
}

var CmdCiDownload = &cobra.Command{
	Use:     "download <job_id>|<job_name> [<file>]",
	Short:   "Download CI artifacts & files",
	Aliases: []string{"dl"},
	Args:    cobra.RangeArgs(1, 2),
	Run:     CiDownload,
}

var CmdCiJobs = &cobra.Command{
	Use:     "jobs [<job_id>|<job_name>]",
	Short:   "List CI jobs or show details",
	Aliases: []string{"job"},
	Args:    cobra.MaximumNArgs(1),
	Run:     CiJobs,
}

var CmdCiPipelines = &cobra.Command{
	Use:     "pipelines [<pipeline_id>]",
	Short:   "List CI pipelines or show details",
	Aliases: []string{"pipeline", "pl"},
	Args:    cobra.MaximumNArgs(1),
	Run:     CiPipelines,
}

var CmdClone = &cobra.Command{
	Use:   "clone <path> [<git options>]",
	Short: "Clone a project",
	Args:  cobra.MinimumNArgs(1),
	Run:   Clone,
}

var CmdMergeRequests = &cobra.Command{
	Use:     "merge-requests [<namespace>]",
	Short:   "List a project or group's merge requests",
	Aliases: []string{"mr"},
	Args:    cobra.MaximumNArgs(1),
	Run:     MergeRequests,
}

var CmdMy = &cobra.Command{
	Use:   "my",
	Short: "List my things",
}

var CmdMyMergeRequests = &cobra.Command{
	Use:     "merge-requests [all|authored|assigned]",
	Short:   "List my merge requests",
	Aliases: []string{"mr"},
	ValidArgs: []string{
		"all\tAssigned and authored (default)",
		"authored\tMerge requests authored by you",
		"assigned\tMerge requests assigned to you",
	},
	Args: args(cobra.MaximumNArgs(1), cobra.OnlyValidArgs),
	Run:  MyMergeRequests,
}

var CmdMyProjects = &cobra.Command{
	Use:   "projects [<search_string>]",
	Short: "List my projects",
	Run:   MyProjects,
	Args:  cobra.MaximumNArgs(1),
}

var CmdOpen = &cobra.Command{
	Use:   "open [<gitlab_ref>]",
	Short: "Open GitLab",
	Long: unindent(`
		Open GitLab

		  gl open                Open the current project
		  gl open !<id>          Open a merge request of the current project
		  gl open #<id>          Open an issue of the current project
		  gl open @<username>    Open a user's page
		  gl open <namespace>    Open a group or project
		  gl open <project>!<id> Open a merge request
		  gl open <project>#<id> Open an issue
		  gl open <group>&<id>   Open an epic
	`[1:]),
	Run:  Open,
	Args: cobra.MaximumNArgs(1),
}

var CmdServer = &cobra.Command{
	Use:   "server",
	Short: "Display or configure servers",
	Run:   Server,
	Args:  cobra.NoArgs,
}

var CmdServerAdd = &cobra.Command{
	Use:   "add <name> <url>",
	Short: "Add a server",
	Run:   ServerAdd,
	Args:  cobra.ExactArgs(2),
}

var CmdServerRemove = &cobra.Command{
	Use:   "remove <name>",
	Short: "Remove a server",
	Run:   ServerRemove,
	Args:  cobra.ExactArgs(1),
}

var CmdServerSetUrl = &cobra.Command{
	Use:   "set-url <name> <url>",
	Short: "Modify a server's URL",
	Run:   ServerSetUrl,
	Args:  cobra.ExactArgs(2),
}

var CmdServerSetToken = &cobra.Command{
	Use:   "set-token",
	Short: "Set the token",
}

var CmdServerSetTokenNetrc = &cobra.Command{
	Use:   "netrc <name> [<token>]",
	Short: "Write the token to your netrc",
	Args:  cobra.RangeArgs(1, 2),
	Run:   ServerSetTokenNetrc,
}
