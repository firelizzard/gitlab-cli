package main

import (
	"archive/zip"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
)

func ciLoad() (string, string) {
	if Flags.Project != "" {
		require(&api)
		return Flags.Project, Flags.Ref
	}

	require(&curRepo, &api)
	if curRepo.Server == nil {
		fatal(ExitUsage, "--project required: not in a git repository")
	}

	switch Flags.Ref {
	case "*":
		return curRepo.Project, ""
	case "":
		return curRepo.Project, curRepo.Branch
	default:
		return curRepo.Project, Flags.Ref
	}
}

func ciGetArtifactsZip(project string, job int) (*zip.Reader, error) {
	r, resp, err := api.Jobs.GetJobArtifacts(project, job)
	apiError(resp, &err)
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	} else if int64(len(b)) < resp.ContentLength {
		return nil, io.ErrUnexpectedEOF
	}

	return zip.NewReader(bytes.NewReader(b), resp.ContentLength)
}

func ciLatestPipeline(project, ref string) int {
	list := new(gitlab.ListProjectPipelinesOptions)
	list.PerPage = 1
	if ref != "" {
		list.Ref = &ref
	}

	pls, resp, err := api.Pipelines.ListProjectPipelines(project, list)
	apiError(resp, &err)
	if err != nil {
		fatal(ExitErrQuery, "Project %s latest pipeline query failed: %v", project, err)
	} else if len(pls) == 0 {
		fatal(ExitErrQuery, "Project %s has no pipelines", project)
	}

	return pls[0].ID
}

func ciJobsFilter(jobs []*gitlab.Job, name string) []*gitlab.Job {
	for i := len(jobs) - 1; i >= 0; i-- {
		if jobs[i].Name != name {
			jobs = append(jobs[:i], jobs[i+1:]...)
		}
	}
	return jobs
}

func ciParsePipelineID(idStr, project, ref string) int {
	if id, err := strconv.ParseInt(idStr, 10, 64); err == nil {
		return int(id)
	}

	if idStr == "latest" {
		return ciLatestPipeline(project, ref)
	}

	fatal(ExitUsage, "Invalid pipeline ID %q", idStr)
	return -1
}

func ciParseJobID(idStr, project, ref, pipeline string) int {
	if id, err := strconv.ParseInt(idStr, 10, 64); err == nil {
		return int(id)
	}

	if pipeline == "" {
		fatal(ExitUsage, "Pipeline must be specified when looking up a job by name")
	}

	opts := new(gitlab.ListJobsOptions)
	pid := ciParsePipelineID(pipeline, project, ref)
	jobs := []*gitlab.Job{}
	apiQueryPages(&opts.ListOptions, func() *APIResp {
		j, resp, err := api.Jobs.ListPipelineJobs(project, pid, opts)
		apiError(resp, &err)
		if err != nil {
			fatal(ExitErrQuery, "Project %s pipeline %d jobs query failed: %v", project, pid, err)
		}

		for _, j := range j {
			if j.Name == idStr {
				jobs = append(jobs, j)
			}
		}

		return resp
	})

	if len(jobs) > 1 {
		fatal(ExitErrQuery, "Project %s pipeline %d has multiple jobs named %q", project, pid, idStr)
	} else if len(jobs) == 0 {
		fatal(ExitErrQuery, "Project %s pipeline %d has no job named %q", project, pid, idStr)
	}

	return jobs[0].ID
}

func ciPrintPipelines(pls []*gitlab.PipelineInfo) {
	switch Flags.Format {
	case FormatHuman:
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		defer w.Flush()

		for _, pl := range pls {
			fmt.Fprintf(w, "Pipeline %d\t(%s - %s)\tstatus is %s\t[%s]\n", pl.ID, pl.Ref, pl.SHA[:6], pl.Status, pl.WebURL)
		}

	case FormatColumn:
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		defer w.Flush()

		fmt.Fprintln(w, "ID\tREF\tSHA\tSTATUS\tURL")
		for _, pl := range pls {
			fmt.Fprintf(w, "%d\t%s\t%s\t%s\t%s\n", pl.ID, pl.Ref, pl.SHA, pl.Status, pl.WebURL)
		}

	case FormatJSON:
		dumpJSON(pls)

	default:
		fatal(ExitUsage, "Format %v is not valid for this command", Flags.Format.String())
	}
}

func ciPrintJobs(jobs []*gitlab.Job) {
	switch Flags.Format {
	case FormatHuman:
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		defer w.Flush()

		for _, job := range jobs {
			fmt.Fprintf(w, "Job %s\t(%d)\tof pipeline %d (%s - %s)\tstatus is %s\t[%s]\n", job.Name, job.ID, job.Pipeline.ID, job.Pipeline.Ref, job.Pipeline.Sha[:6], job.Status, job.WebURL)
		}

	case FormatColumn:
		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		defer w.Flush()

		fmt.Fprintln(w, "NAME\tID\tPIPELINE\tREF\tSHA\tSTATUS\tURL")
		for _, job := range jobs {
			fmt.Fprintf(w, "%s\t%d\t%d\t%s\t%s\t%s\t%s\n", job.Name, job.ID, job.Pipeline.ID, job.Pipeline.Ref, job.Pipeline.Sha, job.Status, job.WebURL)
		}

	case FormatJSON:
		dumpJSON(jobs)

	default:
		fatal(ExitUsage, "Format %v is not valid for this command", Flags.Format.String())
	}
}

func ciPrintPipeline(pl *gitlab.Pipeline) {
	switch Flags.Format {
	case FormatHuman:
		fmt.Printf("Pipeline %d status is %s\n", pl.ID, pl.Status)

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		fmt.Fprintf(w, "User\t%s (%s)\n", pl.User.Name, pl.User.Username)
		fmt.Fprintf(w, "Ref\t%s\n", pl.Ref)
		fmt.Fprintf(w, "SHA\t%s\n", pl.SHA)
		w.Flush()

		fmt.Println(pl.WebURL)

	case FormatJSON:
		dumpJSON(pl)

	default:
		fatal(ExitUsage, "Format %v is not valid for this command", Flags.Format.String())
	}
}

func ciPrintJob(job *gitlab.Job) {
	switch Flags.Format {
	case FormatHuman:
		fmt.Printf("Job %s (%d) status is %s\n", job.Name, job.ID, job.Status)

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		fmt.Fprintf(w, "Pipeline\t%d\n", job.Pipeline.ID)
		fmt.Fprintf(w, "User\t%s (%s)\n", job.User.Name, job.User.Username)
		fmt.Fprintf(w, "Ref\t%s\n", job.Ref)
		fmt.Fprintf(w, "SHA\t%s\n", job.Pipeline.Sha)
		fmt.Fprintln(w, "Artifacts")
		for _, a := range job.Artifacts {
			fmt.Fprintf(w, "  %s\t%s\n", a.Filename, formatBytesSI(a.Size))
		}
		w.Flush()

		fmt.Println(job.WebURL)

	case FormatJSON:
		dumpJSON(job)

	default:
		fatal(ExitUsage, "Format %v is not valid for this command", Flags.Format.String())
	}
}

func ciPrintArtifacts(zip *zip.Reader) {
	switch Flags.Format {
	case FormatHuman, FormatColumn:
		fmtSize := formatBytesSI
		if Flags.Format == FormatColumn {
			fmtSize = func(n int) string { return strconv.FormatInt(int64(n), 10) }
		}

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
		defer w.Flush()

		fmt.Fprintln(w, "NAME\tSIZE")
		for _, f := range zip.File {
			fmt.Fprintf(w, "%s\t%s\n", f.Name, fmtSize(int(f.UncompressedSize)))
		}

	case FormatJSON:
		dumpJSON(zip.File)

	default:
		fatal(ExitUsage, "Format %v is not valid for this command", Flags.Format.String())
	}
}

func CiPipelines(_ *cobra.Command, args []string) {
	project, ref := ciLoad()

	if len(args) > 0 {
		id := ciParsePipelineID(args[0], project, ref)

		pl, resp, err := api.Pipelines.GetPipeline(project, int(id))
		apiError(resp, &err)
		if err != nil {
			fatal(ExitErrQuery, "Project %s pipeline %d query failed: %v", project, id, err)
		}

		ciPrintPipeline(pl)
		return
	}

	list := new(gitlab.ListProjectPipelinesOptions)
	if ref != "" {
		list.Ref = &ref
	}

	apiQueryPages(&list.ListOptions, func() *gitlab.Response {
		pls, resp, err := api.Pipelines.ListProjectPipelines(project, list)
		apiError(resp, &err)
		if err != nil {
			fatal(ExitErrQuery, "Project %s pipelines query failed: %v", project, err)
		}

		ciPrintPipelines(pls)
		return resp
	})
}

func CiJobs(_ *cobra.Command, args []string) {
	project, ref := ciLoad()

	if len(args) > 0 {
		if id, err := strconv.ParseInt(args[0], 10, 64); err == nil {
			job, resp, err := api.Jobs.GetJob(project, int(id))
			apiError(resp, &err)
			if err != nil {
				fatal(ExitErrQuery, "Project %s job %d query failed: %v", project, id, err)
			}

			ciPrintJob(job)
			return
		}
	}

	type Jobs = []*gitlab.Job
	var id int
	var fn func() (Jobs, *APIResp, error)
	list := new(gitlab.ListJobsOptions)

	if Flags.Pipeline == "" {
		fn = func() (Jobs, *APIResp, error) { return api.Jobs.ListProjectJobs(project, list) }
	} else {
		id = ciParsePipelineID(Flags.Pipeline, project, ref)
		fn = func() (Jobs, *APIResp, error) { return api.Jobs.ListPipelineJobs(project, id, list) }
	}

	apiQueryPages(&list.ListOptions, func() *gitlab.Response {
		jobs, resp, err := fn()
		apiError(resp, &err)
		if err != nil {
			if Flags.Pipeline == "" {
				fatal(ExitErrQuery, "Project %s jobs query failed: %v", project, err)
			} else {
				fatal(ExitErrQuery, "Project %s pipeline %d jobs query failed: %v", project, id, err)
			}
		}

		if len(args) > 0 {
			jobs = ciJobsFilter(jobs, args[0])
		}

		ciPrintJobs(jobs)
		return resp
	})
}

func CiArtifacts(_ *cobra.Command, args []string) {
	project, ref := ciLoad()

	id := ciParseJobID(args[0], project, ref, Flags.Pipeline)
	zip, err := ciGetArtifactsZip(project, id)
	if err != nil {
		fatal(ExitErrQuery, "Project %s job %d artifacts query failed: %v", project, Flags.ID, err)
	}

	ciPrintArtifacts(zip)
}

func CiDownload(_ *cobra.Command, args []string) {
	project, ref := ciLoad()
	id := ciParseJobID(args[0], project, ref, Flags.Pipeline)

	var r io.Reader
	var output string

	if len(args) < 2 {
		z, resp, err := api.Jobs.GetJobArtifacts(project, id)
		apiError(resp, &err)
		if err != nil {
			fatal(ExitErrQuery, "Project %s job %d artifacts query failed: %v", project, Flags.ID, err)
		}

		r, output = z, "artifacts.zip"

	} else {
		z, err := ciGetArtifactsZip(project, id)
		if err != nil {
			fatal(ExitErrQuery, "Project %s job %d artifacts query failed: %v", project, Flags.ID, err)
		}

		var file *zip.File
		var found bool
		for _, file = range z.File {
			if file.Name == args[1] {
				found = true
				break
			}
		}

		if !found {
			fatal(ExitErrQuery, "Project %s job %d artifacts does not contain %q", project, Flags.ID, args[1])
		}

		rc, err := file.Open()
		if err != nil {
			fatal(ExitErrQuery, "Failed to read zip entry: %v", err)
		}
		defer rc.Close()
		r, output = rc, file.Name
	}

	if Flags.Output != "" {
		output = Flags.Output
	}

	f, err := os.Create(output)
	if err != nil {
		fatal(ExitErrQuery, "%v", err)
	}
	defer f.Close()

	_, err = io.Copy(f, r)
	if err != nil {
		fatal(ExitErrQuery, "%v", err)
	}
}
