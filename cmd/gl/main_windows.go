package main

import (
	"errors"
	"fmt"
	"os/exec"

	"github.com/spf13/cobra"
)

var CmdServerSetTokenWincred = &cobra.Command{
	Use:   "wincred <name> [<token>]",
	Short: "Write the token to the Windows Credential Manager",
	Args:  cobra.RangeArgs(1, 2),
	Run:   ServerSetTokenWincred,
}

func init() {
	CmdServerSetToken.AddCommand(CmdServerSetTokenWincred)

	CmdDebug.AddCommand(&cobra.Command{
		Use:  "wincred-read <name>",
		Args: cobra.ExactArgs(1),
		Run:  debugWincredRead,
	})
}

func loadServerTokenPlatform() (string, bool) {
	if !server.CredentialStore.TryWincred() {
		return "", false
	}

	cred, err := winCredRead(server.URL, wincredGeneric)
	if err == nil {
		defer cred.Free()
		return string(cred.CredentialsBlob()), true
	} else if !errors.Is(err, ErrNotFound) {
		fatal(ExitErrWinCred, "%v", err)
	}

	return "", false
}

func open(url string) {
	err := exec.Command("start", "", url).Run()
	if err != nil {
		fatal(ExitErrCommand, "Failed to open URL: %v", err)
	}
}

func ServerSetTokenWincred(_ *cobra.Command, args []string) {
	server, token := setTokenLoad(args)

	cred := new(wincred)
	cred.persist = wincredPersistLocalMachine
	cred.typeCode = wincredGeneric
	cred.SetTargetName(server.URL)
	cred.SetUserName("Private Token")
	cred.SetCredentialsBlob([]byte(token))

	err := cred.Write()
	if err != nil {
		fatal(ExitErrWinCred, "%v", err)
	}
}

func debugWincredRead(_ *cobra.Command, args []string) {
	cred, err := winCredRead(args[0], wincredGeneric)
	if err != nil {
		fatal(ExitErrWinCred, "%v", err)
	}
	defer cred.Free()

	fmt.Println(cred.UserName())
	fmt.Println(string(cred.CredentialsBlob()))
}
