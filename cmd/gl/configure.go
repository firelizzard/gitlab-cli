package main

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

func Server(*cobra.Command, []string) {
	require(&config)

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
	defer w.Flush()

	if config.Server.Default == "" {
		for _, s := range config.Servers {
			fmt.Fprintf(w, "%s\t%s\n", s.Name, s.URL)
		}
	} else {
		for _, s := range config.Servers {
			if s.Name == config.Server.Default {
				fmt.Fprintf(w, "* %s\t%s\n", s.Name, s.URL)
			} else {
				fmt.Fprintf(w, "  %s\t%s\n", s.Name, s.URL)
			}
		}
	}
}

func ServerAdd(_ *cobra.Command, args []string) {
	require(&config)
	ensureURL(&args[1])

	if config.Servers.Get(args[0]) != nil {
		fatal(ExitExistingRecord, "Server %q already exists", args[0])
	}

	config.Servers.Add(args[0], args[1])
	saveConfig()
}

func ServerRemove(_ *cobra.Command, args []string) {
	require(&config)

	if config.Servers.Remove(args[0]) == nil {
		fatal(ExitErrConfiguration, "Server %q does not exist", args[0])
	}

	saveConfig()
}

func ServerSetUrl(_ *cobra.Command, args []string) {
	require(&config)
	ensureURL(&args[1])

	server = config.Servers.Get(args[0])
	if server == nil {
		fatal(ExitErrConfiguration, "Server %q does not exist", args[0])
	}

	server.URL = args[1]
	saveConfig()
}

func ServerSetTokenNetrc(_ *cobra.Command, args []string) {
	require(&config, &netRC)

	var name = args[0]
	var token string

	if len(args) > 1 {
		token = args[1]
	} else {
		print("Token: ")
		token = readSecret()
	}

	server = config.Servers.Get(name)
	if server == nil {
		fatal(ExitErrConfiguration, "Server %q does not exist\n", args[0])
	}

	u := mustBeURL(server.URL)
	m := netRC.FindMachine(u.Host)
	if m == nil || m.IsDefault() {
		netRC.NewMachine(u.Host, "", token, "")
	} else {
		m.UpdatePassword(token)
	}

	b, err := netRC.MarshalText()
	if err != nil {
		fatal(ExitErrSaveConfig, "Faled to marshal netrc: %v", err)
	}

	f, err := os.Create(netrcPath)
	if err != nil {
		fatal(ExitErrSaveConfig, "Faled open %s: %v", netrcPath, err)
	}
	defer f.Close()

	_, err = f.Write(b)
	if err != nil {
		fatal(ExitErrSaveConfig, "Faled write %s: %v", netrcPath, err)
	}
}
