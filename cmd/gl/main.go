package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"reflect"
	"regexp"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/firelizzard/gitlab-cli/configure"
	"golang.org/x/crypto/ssh/terminal"
	"golang.org/x/text/language"
	"golang.org/x/text/search"
)

const (
	ExitOK = iota
	ExitUsage
	ExitErrInitialization
	ExitErrConfiguration
	ExitErrCommand
	ExitErrQuery
	ExitErrSaveConfig
	ExitBadValue
	ExitExistingRecord
	ExitNoRecord
	ExitNoRepository
)

const (
	ExitErrKeychain = iota + 64
	ExitErrWinCred
	ExitErrGnomeSS
)

var (
	ErrNotFound = errors.New("not found")
)

type APIResp = gitlab.Response

var bytesSI = []string{"B", "kiB", "MiB", "GiB", "TiB"}

func main() {
	CmdMain.Execute()
}

func usage(cmd *cobra.Command) {
	cmd.Usage()
	os.Exit(ExitUsage)
}

func fatal(code int, format string, args ...interface{}) {
	fmt.Printf(format+"\n", args...)
	os.Exit(code)
}

func must(err error, code int, format string, args ...interface{}) {
	if err != nil {
		fatal(code, format, append([]interface{}{err}, args...)...)
	}
}

func readSecret() string {
	fd := int(syscall.Stdin)
	s, err := terminal.MakeRaw(fd)
	if err != nil {
		fatal(ExitErrCommand, "%v", err)
	}
	defer terminal.Restore(0, s)

	b, err := terminal.ReadPassword(fd)
	println()
	if err != nil {
		fatal(ExitErrCommand, "%v", err)
	}

	return string(b)
}

func preMain(*cobra.Command, []string) {
	if os.Getenv("CI") != "true" || !Flags.CI || Flags.Server != "" {
		return
	}

	server = &configure.Server{
		Name: "ci",
		URL:  os.Getenv("CI_SERVER_URL"),
	}
	serverToken = os.Getenv("CI_JOB_TOKEN")

	var err error
	api, err = gitlab.NewJobTokenClient(serverToken, gitlab.WithBaseURL(server.URL))
	if err != nil {
		fatal(ExitErrInitialization, "Failed to create API client: %v", err)
	}

	if Flags.Project != "" {
		return
	}

	curRepo = &RepoData{
		Server:    server,
		Directory: os.Getenv("CI_PROJECT_DIR"),
		Project:   os.Getenv("CI_PROJECT_ID"),
		Branch:    os.Getenv("CI_COMMIT_REF_NAME"),
	}

	if Flags.Pipeline == "" {
		Flags.Pipeline = os.Getenv("CI_PIPELINE_ID")
	}
}

func setTokenLoad(args []string) (*configure.Server, string) {
	require(&config)

	var name = args[0]
	var token string

	if len(args) > 1 {
		token = args[1]
	} else {
		print("Token: ")
		token = readSecret()
	}

	server := config.Servers.Get(name)
	if server == nil {
		fatal(ExitErrConfiguration, "Server %q does not exist\n", args[0])
	}

	return server, token
}

func apiError(resp *gitlab.Response, err *error) {
	var eresp *gitlab.ErrorResponse
	if *err == nil || !errors.As(*err, &eresp) {
		return
	}

	if Flags.Debug {
		fmt.Fprintln(os.Stderr, resp.Request.URL)
		for name, value := range resp.Request.Header {
			fmt.Fprintf(os.Stderr, "%s: %s\n", name, value)
		}
	}

	var x struct{ Message string }
	if e := json.Unmarshal(eresp.Body, &x); e == nil {
		*err = errors.New(x.Message)
	} else {
		*err = errors.New(eresp.Message)
	}
}

func isLF(r rune) bool { return r == '\n' }

func join(sep string, s ...string) string { return strings.Join(s, sep) }

func formatBytesSI(n int) string {
	i := 0
	for n > 1024 && i < len(bytesSI)-1 {
		n /= 1024
		i++
	}
	return fmt.Sprintf("%d %s", n, bytesSI[i])
}

func mustBeURL(s string) *url.URL {
	u, err := url.Parse(s)
	if err != nil {
		fatal(ExitBadValue, "%q is not a URL: %v", s, err)
	}

	return u
}

func ensureURL(s *string) {
	u := mustBeURL(*s)
	if u.Scheme == "" {
		u.Scheme = "https"
		*s = u.String()
	}
}

func dumpJSON(v interface{}) {
	err := json.NewEncoder(os.Stdout).Encode(v)
	if err != nil {
		fatal(ExitBadValue, "Failed to JSON encode output: %v", v)
	}
}

func compileMatcher(s string) func(string) bool {
	m := search.New(language.Und, search.Loose)
	p := m.CompileString(s)
	return func(s string) bool {
		a, b := p.IndexString(s)
		return a >= 0 && b >= 0
	}
}

type gqlVars map[string]interface{}

func (m1 gqlVars) merge(m2 gqlVars) gqlVars {
	m3 := make(gqlVars, len(m1)+len(m2))

	for k, v := range m1 {
		m3[k] = v
	}

	for k, v := range m2 {
		m3[k] = v
	}

	return m3
}

type gqlPageInfo struct {
	EndCursor   string
	HasNextPage bool
}

func graphQuery(name, query string, vars gqlVars, respData interface{}) {
	// let the caller determine what server to query
	require(&serverToken)

	var respBody = struct {
		Data   interface{}
		Errors []struct {
			Message string
		}
	}{Data: respData}

	reqBody, err := json.Marshal(struct {
		Query     string  `json:"query"`
		Variables gqlVars `json:"variables"`
	}{
		Query:     query,
		Variables: vars,
	})
	if err != nil {
		fatal(ExitErrQuery, "GraphQL %s: failed to encode query: %v\n", name, err)
	}

	rq, err := http.NewRequest(http.MethodPost, server.URL+"/api/graphql", bytes.NewReader(reqBody))
	if err != nil {
		fatal(ExitErrQuery, "GraphQL %s: failed to create request: %v\n", name, err)
	}

	rq.Header.Add("Authorization", "Bearer "+serverToken)
	rq.Header.Set("Content-Type", "application/json; charset=utf-8")
	rq.Header.Set("Accept", "application/json; charset=utf-8")

	resp, err := http.DefaultClient.Do(rq)
	if err != nil {
		fatal(ExitErrQuery, "GraphQL %s: failed to execute request: %v\n", name, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		fatal(ExitErrQuery, "GraphQL %s: request failed: %s", name, resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fatal(ExitErrQuery, "GraphQL %s: failed to read response: %v\n", name, err)
	}

	err = json.Unmarshal(b, &respBody)
	if err != nil {
		fatal(ExitErrQuery, "GraphQL %s: failed to decode response: %v\n", name, err)
	}

	if len(respBody.Errors) == 0 {
		return
	}

	errs := "GraphQL %s failed:"
	for _, err := range respBody.Errors {
		errs += "\n\t" + err.Message
	}
	fatal(ExitErrQuery, errs+"\n", name)
}

func graphQueryPages(name, query string, vars gqlVars, process interface{}) {
	rp := reflect.ValueOf(process)
	rpt := rp.Type()
	if rpt.Kind() != reflect.Func || rpt.NumIn() != 1 || rpt.NumOut() != 1 || rpt.Out(0) != reflect.TypeOf(gqlVars{}) {
		panic("process must be func(data T) gqlVars")
	}

	for {
		rd := reflect.New(rpt.In(0))
		graphQuery(name, query, vars, rd.Interface())

		rv := rp.Call([]reflect.Value{rd.Elem()})
		vars = rv[0].Interface().(gqlVars)
		if vars == nil {
			break
		}

	}
}

func apiQueryPages(lopts *gitlab.ListOptions, query func() *APIResp) {
	for resp := query(); resp != nil && resp.NextPage > 0; resp = query() {
		lopts.Page = resp.NextPage
	}
}

func saveConfig() {
	if err := config.Save(); err != nil {
		fatal(ExitErrSaveConfig, "Failed to save config: %v\n", err)
	}
}

var currentUserName string

func whoami() string {
	if currentUserName != "" {
		return currentUserName
	}

	var q struct {
		CurrentUser *struct {
			Username string
		}
	}
	graphQuery("current user", `{
		currentUser {
			username
		}
	}`, nil, &q)

	if q.CurrentUser == nil {
		fatal(ExitNoRecord, "Authentication failed")
	}

	currentUserName = q.CurrentUser.Username
	return currentUserName
}

func Open(_ *cobra.Command, args []string) {
	if len(args) == 0 || len(args[0]) == 0 {
		require(&curRepo, &server)
		if curRepo.Server == nil {
			open(server.URL)
		} else {
			open(path.Join(server.URL, curRepo.Project))
		}
		return
	} else {
		require(&server)
	}

	m := regexp.MustCompile(`^([-._/\w]+)?([!#&]\d+)?$|^(@[-._\w]+)$`).FindStringSubmatch(args[0])
	if len(m) == 0 {
		fatal(ExitUsage, "Invalid reference: %q", args[0])
	}

	if len(m[3]) > 0 {
		open(path.Join(server.URL, m[3][1:]))
		return
	}

	var isProject bool
	namespace, ref := m[1], m[2]
	if len(namespace) == 0 {
		require(&curRepo)
		if curRepo.Server == nil {
			fatal(ExitUsage, "Not in a git repository")
		}
		namespace, isProject = curRepo.Project, true

	} else if ref == "" {
		open(path.Join(server.URL, namespace))
		return

	} else {
		var q struct{ Project, Group *struct{} }
		graphQuery("namespace", `query($path: ID!) {
			project(fullPath: $path) { id }
			group(fullPath: $path) { id }
		}`, gqlVars{"path": namespace}, &q)
		if q.Project != nil {
			isProject = true
		} else if q.Group != nil {
			isProject = false
		} else {
			fatal(ExitNoRecord, "Could not read namespace %q", namespace)
		}
	}

	switch ref[0] {
	case '!':
		if isProject {
			open(path.Join(server.URL, namespace, "-/merge_requests", ref[1:]))
		} else {
			fatal(ExitUsage, "Groups do not have merge requests")
		}
	case '#':
		if isProject {
			open(path.Join(server.URL, namespace, "-/issues", ref[1:]))
		} else {
			fatal(ExitUsage, "Groups do not have issues")
		}
	case '&':
		if isProject {
			fatal(ExitUsage, "Groups do not have epics")
		} else {
			open(path.Join(server.URL, "groups", namespace, "-/epics", ref[1:]))
		}
	}
}
