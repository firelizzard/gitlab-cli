package main

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

func AllProjects(_ *cobra.Command, args []string) {
	require(&server)

	vars := gqlVars{}
	if len(args) > 0 {
		vars["search"] = args[0]
	}

	graphQueryPages("projects", `query Projects($search: String, $cursor: String) {
		projects(search: $search, after: $cursor) {
			nodes {
				nameWithNamespace
				fullPath
				webUrl
			}

			pageInfo {
				endCursor
				hasNextPage
			}
		}
	}`, vars, func(q struct {
		Projects struct {
			Nodes []struct {
				NameWithNamespace, FullPath, WebUrl string
			}
			PageInfo gqlPageInfo
		}
	}) gqlVars {
		if Flags.Verbose {
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
			defer w.Flush()

			for _, p := range q.Projects.Nodes {
				fmt.Fprintf(w, "%s\t%s\n", p.NameWithNamespace, p.WebUrl)
			}
		} else {
			for _, p := range q.Projects.Nodes {
				fmt.Printf("%s (%s)\n", p.NameWithNamespace, p.FullPath)
			}
		}

		if !q.Projects.PageInfo.HasNextPage {
			return nil
		}

		return vars.merge(gqlVars{"cursor": q.Projects.PageInfo.EndCursor})
	})
}

func MyProjects(_ *cobra.Command, args []string) {
	require(&server)

	filter := func(name, path, description string) bool { return true }
	if len(args) > 0 && args[0] != "" {
		m := compileMatcher(args[0])
		filter = func(name, path, description string) bool {
			return m(name) || m(path) || m(description)
		}
	}

	me := whoami()

	vars := gqlVars{"path": me}
	graphQueryPages("projects", `query Projects($path: ID!) {
		namespace(fullPath: $path) {
			projects {
				nodes {
					name
					path
					description
					webUrl
				}

				pageInfo {
					endCursor
					hasNextPage
				}
			}
		}
	}`, vars, func(qNS struct {
		Namespace *struct {
			Projects struct {
				Nodes []struct {
					Name, Path, Description, WebUrl string
				}
				PageInfo gqlPageInfo
			}
		}
	}) gqlVars {
		if qNS.Namespace == nil {
			fatal(ExitNoRecord, "Could not read namespace %q", me)
		}

		if Flags.Verbose {
			w := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
			defer w.Flush()

			for _, p := range qNS.Namespace.Projects.Nodes {
				if !filter(p.Name, p.Path, p.Description) {
					continue
				}
				fmt.Fprintf(w, "%s\t%s\n", p.Name, p.WebUrl)
			}
		} else {
			for _, p := range qNS.Namespace.Projects.Nodes {
				if !filter(p.Name, p.Path, p.Description) {
					continue
				}
				if p.Name == p.Path {
					fmt.Printf("%s\n", p.Name)
				} else {
					fmt.Printf("%s (%s)\n", p.Name, p.Path)
				}
			}
		}

		if !qNS.Namespace.Projects.PageInfo.HasNextPage {
			return nil
		}

		return vars.merge(gqlVars{"cursor": qNS.Namespace.Projects.PageInfo.EndCursor})
	})
}
