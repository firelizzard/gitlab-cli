package main

import (
	"fmt"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

func MergeRequests(cmd *cobra.Command, args []string) {
	var fromRepo bool
	var branch string

	vars := gqlVars{}
	if len(args) > 0 {
		require(&server)
		vars["path"] = args[0]
	} else if path, br, ok := gitCurrentProject(); ok {
		vars["path"], branch, fromRepo = path, br, true
	} else {
		fatal(ExitUsage, "Namespace argument required: not in a git repository")
	}

	if Flags.State != "" {
		vars["state"] = Flags.State
	} else {
		vars["state"] = "opened"
	}

	if Flags.Authored {
		vars["author"] = whoami()
	} else if Flags.Assigned {
		vars["assignee"] = whoami()
	}

	type mergeRequests struct {
		Nodes []struct {
			IID, Title, WebUrl string
			SourceBranch       string
			Project            struct {
				FullPath string
			}
		}
		PageInfo gqlPageInfo
	}
	const mrArgs = `authorUsername: $author, assigneeUsername: $assignee, state: $state`
	const mrQuery = `{
		nodes {
			iid
			title
			webUrl
			sourceBranch
			project {
				fullPath
			}
		}

		pageInfo {
			endCursor
			hasNextPage
		}
	}`

	graphQueryPages("merge requests", `query MergeRequests($path: ID!, $state: MergeRequestState!, $author: String, $assignee: String, $cursor: String) {
		project(fullPath: $path) { mergeRequests(`+mrArgs+`, after: $cursor)`+mrQuery+` }
		group(fullPath: $path) { mergeRequests(`+mrArgs+`, after: $cursor)`+mrQuery+` }
	}`, vars, func(q struct {
		Project, Group *struct{ MergeRequests mergeRequests }
	}) gqlVars {
		var scope mergeRequests
		if q.Project != nil {
			scope = q.Project.MergeRequests
		} else if q.Group != nil {
			scope = q.Group.MergeRequests
		} else {
			fatal(ExitNoRecord, "Cannot read namespace %q", vars["path"])
		}

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
		defer w.Flush()

		for _, mr := range scope.Nodes {
			if fromRepo {
				cur := ' '
				if mr.SourceBranch == branch {
					cur = '*'
				}
				fmt.Fprintf(w, "%c%s\t%s\t%s\n", cur, mr.IID, mr.SourceBranch, mr.Title)
			} else {
				fmt.Fprintf(w, "%s\t%s!%s\t%s\n", mr.Title, mr.Project.FullPath, mr.IID, mr.WebUrl)
			}
		}

		if !scope.PageInfo.HasNextPage {
			return nil
		}

		return gqlVars{"cursor": scope.PageInfo.EndCursor}.merge(vars)
	})
}

func MyMergeRequests(cmd *cobra.Command, args []string) {
	require(&server)

	vars := gqlVars{}
	if Flags.All {
		vars["state"] = "all"
	} else if Flags.State != "" {
		vars["state"] = Flags.State
	} else {
		vars["state"] = "opened"
	}

	authored := len(args) < 1 || args[0] == "all" || args[0] == "authored"
	assigned := len(args) < 1 || args[0] == "all" || args[0] == "assigned"

	type mergeRequests struct {
		Nodes []struct {
			IID, Title, WebUrl string
			Project            struct {
				FullPath string
			}
		}
		PageInfo gqlPageInfo
	}
	const mrQuery = `{
		nodes {
			iid
			title
			webUrl
			project {
				fullPath
			}
		}

		pageInfo {
			endCursor
			hasNextPage
		}
	}`

	var queryVars = "$state: MergeRequestState!"
	var queryBody string
	if authored {
		queryVars += ", $cAuthored: String"
		queryBody += "authoredMergeRequests(after: $cAuthored, state: $state)" + mrQuery + "\n"
	}
	if assigned {
		queryVars += ", $cAssigned: String"
		queryBody += "assignedMergeRequests(after: $cAssigned, state: $state)" + mrQuery + "\n"
	}

	graphQueryPages("merge requests", `query MergeRequests(`+queryVars+`) {
		currentUser {
			`+queryBody+`
		}
	}`, vars, func(q struct {
		CurrentUser *struct {
			AuthoredMergeRequests, AssignedMergeRequests mergeRequests
		}
	}) gqlVars {
		if q.CurrentUser == nil {
			fatal(ExitNoRecord, "Authentication failed")
		}

		w := tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
		defer w.Flush()

		sets := []mergeRequests{
			q.CurrentUser.AuthoredMergeRequests,
			q.CurrentUser.AssignedMergeRequests,
		}

		var hasNext bool
		for _, set := range sets {
			if set.PageInfo.HasNextPage {
				hasNext = true
			}

			for _, mr := range set.Nodes {
				fmt.Fprintf(w, "%s\t%s!%s\t%s\n", mr.Title, mr.Project.FullPath, mr.IID, mr.WebUrl)
			}
		}

		if !hasNext {
			return nil
		}

		return gqlVars{
			"cAuthored": q.CurrentUser.AuthoredMergeRequests.PageInfo.EndCursor,
			"cAssigned": q.CurrentUser.AssignedMergeRequests.PageInfo.EndCursor,
		}.merge(vars)
	})
}
