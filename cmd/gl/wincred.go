// +build windows

package main

import (
	"errors"
	"fmt"
	"reflect"
	"syscall"
	"unsafe"
)

var (
	modadvapi32 = syscall.NewLazyDLL("advapi32.dll")

	_winCredRead  = modadvapi32.NewProc("CredReadW").Call
	_winCredWrite = modadvapi32.NewProc("CredWriteW").Call
	_winCredFree  = modadvapi32.NewProc("CredFree").Call
	_winCredEnum  = modadvapi32.NewProc("CredEnumerateW").Call

	winErrUnknown = errors.New("unknown error")
)

// https://docs.microsoft.com/en-us/windows/desktop/api/wincred/ns-wincred-_credentialw
type wincredType uint32

type wincredPersist uint32

const (
	wincredGeneric               wincredType = 1
	wincredDomainPassword        wincredType = 2
	wincredDomainCertificate     wincredType = 3
	wincredDomainVisiblePassword wincredType = 4
	wincredGenericCertificate    wincredType = 5
	wincredDomainExtended        wincredType = 6

	wincredPersistSession      wincredPersist = 1
	wincredPersistLocalMachine wincredPersist = 2
	wincredPersistEnterprise   wincredPersist = 3

	// https://docs.microsoft.com/en-us/windows/desktop/Debug/system-error-codes
	winErrOK               = syscall.Errno(0)
	winErrNotFound         = syscall.Errno(1168)
	winErrInvalidParameter = syscall.Errno(87)
)

// https://docs.microsoft.com/en-us/windows/desktop/api/wincred/ns-wincred-_credentialw
type wincred struct {
	flags              uint32
	typeCode           wincredType
	targetName         *uint16
	comment            *uint16
	lastWritten        syscall.Filetime
	credentialBlobSize uint32
	credentialBlob     *byte
	persist            wincredPersist
	attributeCount     uint32
	attributes         uintptr
	targetAlias        *uint16
	userName           *uint16
}

type wincreds []*wincred

func winWStrLen(s *uint16) uintptr {
	if s == nil {
		return 0
	}

	const csize = unsafe.Sizeof(uint16(0))
	for p := s; ; p = (*uint16)(unsafe.Pointer(uintptr(unsafe.Pointer(p)) + csize)) {
		if *p == 0 {
			return (uintptr(unsafe.Pointer(p)) - uintptr(unsafe.Pointer(s))) / csize
		}
	}
}

func utf16PTRToString(s *uint16) string {
	l := winWStrLen(s)
	if l == 0 {
		return ""
	}

	// properly allocate a slice header, then use unsafe magic to update its fields
	var arr []uint16
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&arr))
	hdr.Data = uintptr(unsafe.Pointer(s))
	hdr.Len = int(l)
	hdr.Cap = hdr.Len
	return syscall.UTF16ToString(arr)
}

func mustUTF16PtrFromString(s string) *uint16 {
	p, err := syscall.UTF16PtrFromString(s)
	if err != nil {
		fatal(ExitBadValue, "Failed to convert %q to UTF16: %v", s, err)
	}

	return p
}

func winCredRead(name string, typ wincredType) (*wincred, error) {
	var cred *wincred
	pname, err := syscall.UTF16PtrFromString(name)
	if err != nil {
		return nil, err
	}

	_, _, err = _winCredRead(uintptr(unsafe.Pointer(pname)), uintptr(typ), 0, uintptr(unsafe.Pointer(&cred)))
	switch err {
	case winErrOK:
		return cred, nil
	case winErrNotFound:
		return nil, fmt.Errorf("wincred: %q %w", name, ErrNotFound)
	default:
		return nil, err
	}
}

func winCredEnum(filter string) (wincreds, error) {
	var pfilter *uint16
	var err error
	if filter != "" {
		pfilter, err = syscall.UTF16PtrFromString(filter)
		if err != nil {
			return nil, err
		}
	}

	var count int
	var pcreds uintptr
	_, _, err = _winCredEnum(uintptr(unsafe.Pointer(pfilter)), 0, uintptr(unsafe.Pointer(&count)), uintptr(unsafe.Pointer(&pcreds)))
	if err != winErrOK {
		return nil, err
	}

	// properly allocate a slice header, then use unsafe magic to update its fields
	var creds wincreds
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&creds))
	hdr.Data = pcreds
	hdr.Len = count
	hdr.Cap = count

	return creds, nil
}

func (c *wincred) Free() {
	_winCredFree(uintptr(unsafe.Pointer(c)))
}

func (c wincreds) Free() {
	pcreds := (*reflect.SliceHeader)(unsafe.Pointer(&c)).Data
	_winCredFree(pcreds)
}

func (c *wincred) Write() error {
	_, _, err := _winCredWrite(uintptr(unsafe.Pointer(c)), 0)
	if err == winErrOK {
		return nil
	}
	return err
}

func (c *wincred) Type() wincredType  { return wincredType(c.typeCode) }
func (c *wincred) TargetName() string { return utf16PTRToString(c.targetName) }
func (c *wincred) UserName() string   { return utf16PTRToString(c.userName) }

func (c *wincred) CredentialsBlob() []byte {
	if c.credentialBlob == nil || c.credentialBlobSize == 0 {
		return nil
	}

	var src []byte
	hdr := (*reflect.SliceHeader)(unsafe.Pointer(&src))
	hdr.Data = uintptr(unsafe.Pointer(c.credentialBlob))
	hdr.Len = int(c.credentialBlobSize)
	hdr.Cap = hdr.Len

	dst := make([]byte, len(src))
	copy(dst, src)
	return dst
}

func (c *wincred) SetTargetName(s string) { c.targetName = mustUTF16PtrFromString(s) }
func (c *wincred) SetUserName(s string)   { c.userName = mustUTF16PtrFromString(s) }

func (c *wincred) SetCredentialsBlob(b []byte) {
	c.credentialBlob = &b[0]
	c.credentialBlobSize = uint32(len(b))
}
