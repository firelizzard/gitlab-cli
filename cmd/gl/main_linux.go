package main

import (
	"fmt"
	"os/exec"

	"github.com/spf13/cobra"
)

var CmdServerSetTokenGnome = &cobra.Command{
	Use:   "gnome <name> [<token>]",
	Short: "Write the token to the Gnome Secret Service",
	Args:  cobra.RangeArgs(1, 2),
	Run:   ServerSetTokenGnome,
}

func init() {
	CmdServerSetToken.AddCommand(CmdServerSetTokenGnome)

	CmdDebug.AddCommand(&cobra.Command{
		Use:  "gnome-read [<name>]",
		Args: cobra.MaximumNArgs(1),
		Run:  debugGnomeRead,
	})

	CmdDebug.AddCommand(&cobra.Command{
		Use:  "gnome-create <name>",
		Args: cobra.ExactArgs(1),
		Run:  debugGnomeCreate,
	})
}

func loadServerTokenPlatform() (string, bool) {
	if !server.CredentialStore.TryGnomeSS() {
		return "", false
	}

	gss, err := newGnomeSecretService()
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to open Gnome Secret Service: %v", err)
	}

	secrets, err := gss.Get(server.URL, "Private Token")
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to list secrets: %v", err)
	} else if len(secrets) > 0 {
		return string(secrets[0].Value), true
	}

	return "", false
}

func open(url string) {
	err := exec.Command("xgd-open", url).Run()
	if err != nil {
		fatal(ExitErrCommand, "Failed to open URL: %v", err)
	}
}

func ServerSetTokenGnome(_ *cobra.Command, args []string) {
	server, token := setTokenLoad(args)

	gss, err := newGnomeSecretService()
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to open Gnome Secret Service: %v", err)
	}

	err = gss.Set(server.URL, "Private Token", dbusSecret{
		Parameters:  []byte{},
		Value:       []byte(token),
		ContentType: "text/plain; charset=utf8",
	})
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to write secret: %v", err)
	}
}

func debugGnomeRead(_ *cobra.Command, args []string) {
	gss, err := newGnomeSecretService()
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to open Gnome Secret Service: %v", err)
	}

	var service string
	if len(args) > 0 {
		service = args[0]
	}

	secrets, err := gss.Get(service, "")
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to list secrets: %v", err)
	}

	for _, s := range secrets {
		o := gss.serviceObject(s.Item)

		var label string
		err := dbusCall(o, dbusPropsInterface, "Get", 0, itemInterface, "Label").Store(&label)
		if err != nil {
			fatal(ExitErrGnomeSS, "Failed to get label: %v", err)
		}

		var attrs map[string]string
		err = dbusCall(o, dbusPropsInterface, "Get", 0, itemInterface, "Attributes").Store(&attrs)
		if err != nil {
			fatal(ExitErrGnomeSS, "Failed to get attributes: %v", err)
		}

		delete(attrs, "xdg:schema")
		fmt.Printf("%v:\n  path: %v\n  attrs: %#v\n", label, s.Item, attrs)
	}
}

func debugGnomeCreate(_ *cobra.Command, args []string) {
	gss, err := newGnomeSecretService()
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to open Gnome Secret Service: %v", err)
	}

	path, err := gss.CreateCollection(args[0])
	if err != nil {
		fatal(ExitErrGnomeSS, "Failed to create collection %q: %v", args[0], err)
	}

	fmt.Println(path)
}
