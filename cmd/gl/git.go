package main

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/firelizzard/gitlab-cli/configure"
)

func gitRemoteMatch(server *configure.Server, remotes map[string]string, configuredProject string) ([]string, bool) {
	var paths []string

	// look through all remotes
	for _, url := range remotes {
		// is the remote relative to the server?
		path, ok := server.Rel(url)
		if !ok {
			continue
		}

		// found relative match, unless `git config gitlab-cli.project` is defined
		if configuredProject == "" {
			paths = append(paths, path)
		}

		// found exact match if path equals the configured project
		if configuredProject == path {
			return []string{path}, true
		}
	}

	return paths, false
}

func gitCurrentProject() (path, branch string, ok bool) {
	require(&config)

	var exitErr *exec.ExitError
	b, err := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD").CombinedOutput()
	if errors.As(err, &exitErr) {
		// not in a repository
		return "", "", false
	} else if err != nil {
		fatal(ExitErrCommand, "git rev-parse failed: %v", err)
	}

	if f := bytes.FieldsFunc(b, isLF); len(f) == 1 {
		branch = string(f[0])
	} else {
		panic("wrong number of lines from rev-parse")
	}

	b, err = exec.Command("git", "config", "--list").CombinedOutput()
	if err != nil {
		fatal(ExitErrCommand, "git config --list failed: %v", err)
	}

	var gcliServer, gcliProject string
	remotes := map[string]string{}
	for _, l := range bytes.FieldsFunc(b, isLF) {
		parts := bytes.SplitN(l, []byte{'='}, 2)
		key, value := parts[0], parts[1]

		if string(key) == "gitlab-cli.server" {
			gcliServer = string(value)
			continue
		}

		if string(key) == "gitlab-cli.project" {
			gcliProject = string(value)
			continue
		}

		keyParts := bytes.Split(key, []byte{'.'})
		if len(keyParts) == 3 && string(keyParts[0]) == "remote" && string(keyParts[2]) == "url" {
			remotes[string(keyParts[1])] = string(value)
		}
	}

	if len(config.Servers) == 0 {
		fatal(ExitErrConfiguration, "No servers configured")
	}

	if gcliServer != "" && gcliProject != "" {
		requireServer(gcliServer)
		return gcliProject, branch, true
	}

	if gcliServer == "" {
		gcliServer = Flags.Server
	}
	if gcliServer != "" {
		requireServer(gcliServer)

		paths, _ := gitRemoteMatch(server, remotes, gcliProject)
		if len(paths) == 0 {
			fatal(ExitUsage, "Server %s does not match any remotes", server.Name)
		}
		if len(paths) > 1 {
			fatal(ExitUsage, "Server %s matches multiple remotes", server.Name)
		}
		return paths[0], branch, true
	}

	// compare all servers to all remotes
	var paths [][]string
	for i, s := range config.Servers {
		p, exact := gitRemoteMatch(&s, remotes, gcliProject)
		if exact {
			server = &config.Servers[i]
			return p[0], branch, true
		}

		if len(p) > 0 {
			server = &config.Servers[i]
			paths = append(paths, p)
		}
	}

	if len(paths) == 0 {
		fatal(ExitUsage, "No configured server matches a remote")
	}

	if len(paths) > 1 {
		fatal(ExitUsage, "Please select a server: multiple servers match a remote")
	}

	if len(paths[0]) > 1 {
		fatal(ExitUsage, "Server %s matches multiple remotes", server.Name)
	}

	return paths[0][0], branch, true
}

func Checkout(_ *cobra.Command, args []string) {
	path, _, ok := gitCurrentProject()
	if !ok {
		fatal(ExitNoRepository, "Not in a git repository")
	}

	var q struct {
		Project *struct {
			MergeRequest *struct {
				SourceBranch string
			}
		}
	}
	graphQuery("merge request", `query MergeRequest($project: ID!, $mr: String!) {
		project(fullPath: $project) {
			mergeRequest(iid: $mr) {
				sourceBranch
			}
		}
	}`, gqlVars{"project": path, "mr": strconv.FormatInt(Flags.ID, 10)}, &q)

	if q.Project == nil {
		fatal(ExitNoRecord, "Could not read project %q", path)
	} else if q.Project.MergeRequest == nil {
		fatal(ExitNoRecord, "Could not read merge request %s!%d", path, Flags.ID)
	}

	gitArgs := []string{"checkout", q.Project.MergeRequest.SourceBranch}
	fmt.Println("# git", strings.Join(gitArgs, " "))
	cmd := exec.Command("git", gitArgs...)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	if err := cmd.Run(); err != nil {
		fatal(ExitErrCommand, "%v", err)
	}
}

func Clone(_ *cobra.Command, args []string) {
	require(&server)

	var q struct {
		Project *struct {
			SshUrlToRepo, HttpUrlToRepo string
			Repository                  struct {
				Exists bool
			}
		}
	}
	graphQuery("projects", `query Projects($path: ID!) {
		project(fullPath: $path) {
		  sshUrlToRepo
		  httpUrlToRepo
		  repository {
			exists
		  }
		}
	}`, gqlVars{"path": args[0]}, &q)

	if q.Project == nil {
		fatal(ExitNoRecord, "Could not read project %q", args[0])
	} else if !q.Project.Repository.Exists {
		fatal(ExitNoRecord, "Project %s does not have a repository", args[0])
	}

	var gitArgs = []string{"clone"}
	if q.Project.SshUrlToRepo != "" {
		gitArgs = append(gitArgs, q.Project.SshUrlToRepo)
	} else if q.Project.HttpUrlToRepo != "" {
		gitArgs = append(gitArgs, q.Project.HttpUrlToRepo)
	} else {
		fatal(ExitNoRecord, "No clonable URL for project %s", gitArgs[0])
	}
	gitArgs = append(gitArgs, args[1:]...)

	fmt.Println("# git", strings.Join(gitArgs, " "))
	cmd := exec.Command("git", gitArgs...)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr
	if err := cmd.Run(); err != nil {
		fatal(ExitErrCommand, "%v", err)
	}
}
