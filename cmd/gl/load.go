package main

import (
	"errors"
	"os"
	"path/filepath"
	"reflect"
	"runtime"

	"github.com/bgentry/go-netrc/netrc"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/firelizzard/gitlab-cli/configure"
)

type RepoData struct {
	Server    *configure.Server
	Directory string
	Project   string
	Branch    string
}

var config *configure.FileConfig
var server *configure.Server
var serverToken string
var netRC *netrc.Netrc
var netrcPath string
var api *gitlab.Client
var curRepo *RepoData

func require(ptr ...interface{}) {
	for _, ptr := range ptr {
		if !reflect.ValueOf(ptr).Elem().IsZero() {
			continue
		}

		switch ptr {
		case &config:
			loadConfig()
		case &server:
			loadServer()
		case &serverToken:
			loadServerToken()
		case &netRC:
			loadNetRC()
		case &api:
			loadGitlabApi()
		case &curRepo:
			loadCurrentRepository()
		default:
			panic("required invalid var")
		}
	}
}

func loadConfig() {
	if Flags.ConfigFile == "" {
		fatal(ExitUsage, "Config file not specified")
	}

	var err error
	config, err = configure.LoadFile(Flags.ConfigFile)
	if err == nil {
		return
	} else if !errors.Is(err, os.ErrNotExist) {
		fatal(ExitErrInitialization, "%v", err)
	}

	config, err = configure.NewFile(Flags.ConfigFile)
	if err != nil {
		fatal(ExitErrInitialization, "%v", err)
	}
}

func requireServer(name string) {
	require(&config)

	server = config.Servers.Get(name)
	if server == nil {
		fatal(ExitErrConfiguration, "No server named %q", name)
	}
}

func loadServer() {
	require(&config)

	if Flags.Server != "" {
		requireServer(Flags.Server)
		return
	}

	if os.Getenv("CI") == "true" && os.Getenv("CI_SERVER_URL") != "" {
		server = &configure.Server{
			Name: "ci",
			URL:  os.Getenv("CI_SERVER_URL"),
		}
		return
	}

	if config.Server.Default != "" {
		requireServer(config.Server.Default)
		return
	}

	if len(config.Servers) == 0 {
		fatal(ExitErrConfiguration, "No servers configured")
	}

	if len(config.Servers) == 1 {
		server = &config.Servers[0]
		return
	}

	fatal(ExitUsage, "Please specify a server")
}

func loadNetRC() {
	var err error
	if netrcPath = os.Getenv("NETRC"); netrcPath != "" {
		netRC, err = netrc.ParseFile(netrcPath)
		if err == nil {
			return
		} else if errors.Is(err, os.ErrNotExist) {
			netRC = new(netrc.Netrc)
			return
		} else {
			fatal(ExitErrInitialization, "%v", err)
		}
	}

	dir, err := os.UserHomeDir()
	if err != nil {
		fatal(ExitErrInitialization, "%v", err)
	}

	if runtime.GOOS == "windows" {
		netrcPath = filepath.Join(dir, "_netrc")
		netRC, err = netrc.ParseFile(netrcPath)
		if err == nil {
			return
		} else if err != nil && !errors.Is(err, os.ErrNotExist) {
			fatal(ExitErrInitialization, "%v", err)
		}
		return
	}

	netrcPath = filepath.Join(dir, ".netrc")
	netRC, err = netrc.ParseFile(netrcPath)
	if err == nil {
		return
	} else if err != nil && !errors.Is(err, os.ErrNotExist) {
		fatal(ExitErrInitialization, "%v", err)
	}

	netRC = new(netrc.Netrc)
}

func loadServerToken() {
	require(&server, &netRC)

	if os.Getenv("CI") == "true" && server.URL == os.Getenv("CI_SERVER_URL") && os.Getenv("CI_JOB_TOKEN") != "" {
		serverToken = os.Getenv("CI_JOB_TOKEN")
		return
	}

	u := mustBeURL(server.URL)
	if server.CredentialStore.TryNetRC() {
		m := netRC.FindMachine(u.Host)
		if m != nil {
			serverToken = m.Password
			return
		}
	}

	if t, ok := loadServerTokenPlatform(); ok {
		serverToken = t
		return
	}

	fatal(ExitErrConfiguration, "Cannot determine access token for %s", server.URL)
}

func loadGitlabApi() {
	require(&server, &serverToken)

	var err error
	api, err = gitlab.NewClient(serverToken, gitlab.WithBaseURL(server.URL))
	if err != nil {
		fatal(ExitErrInitialization, "Failed to create API client: %v", err)
	}
}

func loadCurrentRepository() {
	project, branch, ok := gitCurrentProject()
	if !ok {
		curRepo = new(RepoData)
		return
	}

	wd, _ := os.Getwd()
	curRepo = &RepoData{
		Server:    server,
		Directory: wd,
		Project:   project,
		Branch:    branch,
	}
}
