// +build linux

package main

import (
	"fmt"

	"github.com/godbus/dbus"
)

const (
	serviceName          = "org.freedesktop.secrets"
	servicePath          = "/org/freedesktop/secrets"
	serviceInterface     = "org.freedesktop.Secret.Service"
	collectionInterface  = "org.freedesktop.Secret.Collection"
	collectionsInterface = "org.freedesktop.Secret.Service.Collections"
	itemInterface        = "org.freedesktop.Secret.Item"
	sessionInterface     = "org.freedesktop.Secret.Session"
	promptInterface      = "org.freedesktop.Secret.Prompt"
	dbusPropsInterface   = "org.freedesktop.DBus.Properties"

	loginCollectionAlias = "/org/freedesktop/secrets/aliases/default"
	collectionBasePath   = "/org/freedesktop/secrets/collection"
)

type gnomeSecretService struct {
	conn   *dbus.Conn
	object dbus.BusObject
}

type dbusSecret struct {
	Item        dbus.ObjectPath `dbus:"-"`
	Session     dbus.ObjectPath
	Parameters  []byte
	Value       []byte
	ContentType string `dbus:"content_type"`
}

func newGnomeSecretService() (*gnomeSecretService, error) {
	conn, err := dbus.SessionBus()
	if err != nil {
		return nil, err
	}

	obj := conn.Object(serviceName, servicePath)
	return &gnomeSecretService{conn, obj}, nil
}

func dbusClose(session dbus.BusObject) error {
	return session.Call(join(".", sessionInterface, "Close"), 0).Err
}

func dbusCall(obj dbus.BusObject, iface, method string, flags dbus.Flags, args ...interface{}) *dbus.Call {
	return obj.Call(join(".", iface, method), flags, args...)
}

func dbusAttrs(service, username string) map[string]string {
	a := map[string]string{}
	if service != "" {
		a["service"] = service
	}
	if username != "" {
		a["username"] = username
	}
	return a
}

func (s *gnomeSecretService) serviceObject(path dbus.ObjectPath) dbus.BusObject {
	return s.conn.Object(serviceName, path)
}

func (s *gnomeSecretService) serviceCall(path dbus.ObjectPath, iface, method string, flags dbus.Flags, args ...interface{}) *dbus.Call {
	return dbusCall(s.serviceObject(path), iface, method, flags, args...)
}

func (s *gnomeSecretService) CheckCollectionPath(path dbus.ObjectPath) error {
	v, err := s.serviceObject(servicePath).GetProperty(collectionsInterface)
	if err != nil {
		return err
	}

	for _, p := range v.Value().([]dbus.ObjectPath) {
		if p == path {
			return nil
		}
	}

	return fmt.Errorf("%q %w", path, ErrNotFound)
}

func (s *gnomeSecretService) GetLoginCollection() dbus.BusObject {
	path := dbus.ObjectPath(join("/", collectionBasePath, "login"))

	err := s.CheckCollectionPath(path)
	if err != nil {
		path = dbus.ObjectPath(loginCollectionAlias)
	}
	return s.serviceObject(path)
}

func (s *gnomeSecretService) Prompt(prompt dbus.ObjectPath) (bool, dbus.Variant, error) {
	if prompt == dbus.ObjectPath("/") {
		return false, dbus.MakeVariant(""), nil
	}

	err := s.serviceCall(prompt, promptInterface, "Prompt", 0, "").Err
	if err != nil {
		return false, dbus.MakeVariant(""), err
	}

	ch := make(chan *dbus.Signal, 1)
	s.conn.Signal(ch)
	signal := <-ch

	if signal.Name != join(".", promptInterface, "Completed") {
		return false, dbus.MakeVariant(""), nil
	}

	return signal.Body[0].(bool), signal.Body[1].(dbus.Variant), nil
}

func (s *gnomeSecretService) Unlock(col dbus.ObjectPath) error {
	var unlocked []dbus.ObjectPath
	var prompt dbus.ObjectPath
	err := s.serviceCall(servicePath, serviceInterface, "Unlock", 0, []dbus.ObjectPath{col}).Store(&unlocked, &prompt)
	if err != nil {
		return err
	}

	_, v, err := s.Prompt(prompt)
	if err != nil {
		return err
	}

	if collections, ok := v.Value().([]dbus.ObjectPath); ok {
		unlocked = append(unlocked, collections...)
	}

	if len(unlocked) != 1 || (col != loginCollectionAlias && unlocked[0] != col) {
		return fmt.Errorf("failed to unlock collection %q", col)
	}
	return nil
}

func (s *gnomeSecretService) OpenSession() (dbus.BusObject, error) {
	var sessionPath dbus.ObjectPath
	err := s.serviceCall(servicePath, serviceInterface, "OpenSession", 0, "plain", dbus.MakeVariant("")).Store(new(dbus.Variant), &sessionPath)
	if err != nil {
		return nil, err
	}

	return s.serviceObject(sessionPath), nil
}

func (s *gnomeSecretService) Get(service, user string) ([]dbusSecret, error) {
	col := s.GetLoginCollection()
	err := s.Unlock(col.Path())
	if err != nil {
		return nil, err
	}

	var items []dbus.ObjectPath
	err = dbusCall(col, collectionInterface, "SearchItems", 0, dbusAttrs(service, user)).Store(&items)
	if err != nil {
		return nil, err
	}

	if len(items) == 0 {
		return []dbusSecret{}, nil
	}

	session, err := s.OpenSession()
	if err != nil {
		return nil, err
	}
	defer dbusClose(session)

	secrets := make([]dbusSecret, len(items))
	for i := range items {
		secrets[i].Item = items[i]
		err = s.serviceCall(items[i], itemInterface, "GetSecret", 0, session.Path()).Store(&secrets[i])
		if err != nil {
			return nil, err
		}
	}

	return secrets, nil
}

func (s *gnomeSecretService) Set(service, user string, secret dbusSecret) error {
	session, err := s.OpenSession()
	if err != nil {
		return err
	}
	defer dbusClose(session)

	col := s.GetLoginCollection()
	err = s.Unlock(col.Path())
	if err != nil {
		return err
	}

	var prompt dbus.ObjectPath
	secret.Session = session.Path()
	err = dbusCall(col, collectionInterface, "CreateItem", 0, map[string]dbus.Variant{
		join(".", itemInterface, "Label"):      dbus.MakeVariant(fmt.Sprintf("Password for %q on %q", user, service)),
		join(".", itemInterface, "Attributes"): dbus.MakeVariant(dbusAttrs(service, user)),
	}, secret, true).Store(new(dbus.ObjectPath), &prompt)
	if err != nil {
		return err
	}

	_, _, err = s.Prompt(prompt)
	return err
}

func (s *gnomeSecretService) CreateCollection(label string) (dbus.BusObject, error) {
	properties := map[string]dbus.Variant{
		collectionInterface + ".Label": dbus.MakeVariant(label),
	}
	var collection, prompt dbus.ObjectPath
	err := s.object.Call(serviceInterface+".CreateCollection", 0, properties, "").
		Store(&collection, &prompt)
	if err != nil {
		return nil, err
	}

	_, v, err := s.Prompt(prompt)
	if err != nil {
		return nil, err
	}

	if v.String() != "" {
		collection = dbus.ObjectPath(v.String())
	}

	return s.serviceObject(collection), nil
}
