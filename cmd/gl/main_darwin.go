package main

import (
	"os/exec"

	"github.com/keybase/go-keychain"
	"github.com/spf13/cobra"
)

var CmdServerSetTokenKeychain = &cobra.Command{
	Use:   "keychain <name> [<token>]",
	Short: "Write the token to the macOS KeyChain",
	Args:  cobra.RangeArgs(1, 2),
	Run:   ServerSetTokenKeychain,
}

func init() {
	CmdServerSetToken.AddCommand(CmdServerSetTokenKeychain)
}

func open(url string) {
	err := exec.Command("open", url).Run()
	if err != nil {
		fatal(ExitErrCommand, "Failed to open URL: %v", err)
	}
}

func loadServerTokenPlatform() (string, bool) {
	if !server.CredentialStore.TryKeychain() {
		return "", false
	}

	// look for 'Private Token' app-specific password
	query := keychain.NewItem()
	query.SetSecClass(keychain.SecClassGenericPassword)
	query.SetAccount("Private Token")
	query.SetService(server.URL)
	query.SetMatchLimit(keychain.MatchLimitOne)
	query.SetReturnData(true)

	r, err := keychain.QueryItem(query)
	if err != nil {
		fatal(ExitErrKeychain, "%v", err)
	}

	if len(r) == 1 {
		return string(r[0].Data), true
	}

	// look for internet password with any account name
	query = keychain.NewItem()
	query.SetSecClass(keychain.SecClassInternetPassword)
	query.SetService(server.URL)
	query.SetMatchLimit(keychain.MatchLimitOne)
	query.SetReturnData(true)

	r, err = keychain.QueryItem(query)
	if err != nil {
		fatal(ExitErrKeychain, "%v", err)
	}

	if len(r) == 1 {
		return string(r[0].Data), true
	}

	return "", false
}

func ServerSetTokenKeychain(_ *cobra.Command, args []string) {
	server, token := setTokenLoad(args)

	item := keychain.NewItem()
	item.SetSecClass(keychain.SecClassGenericPassword)
	item.SetService(server.URL)
	item.SetAccount("Private Token")
	item.SetLabel("GitLab Private Token")
	item.SetData([]byte(token))
	item.SetAccessible(keychain.AccessibleWhenUnlocked)

	if err := keychain.AddItem(item); err != nil {
		fatal(ExitErrKeychain, "Failed to write keychain: %v", err)
	}
}
