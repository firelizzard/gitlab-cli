module gitlab.com/firelizzard/gitlab-cli

go 1.14

require (
	github.com/bgentry/go-netrc v0.0.0-20140422174119-9fd32a8b3d3d
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/keybase/go-keychain v0.0.0-20200502122510-cda31fe0c86d
	github.com/pelletier/go-toml v1.8.1
	github.com/spf13/cobra v1.1.1
	github.com/spf13/pflag v1.0.5
	github.com/xanzy/go-gitlab v0.39.0
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59
	golang.org/x/text v0.3.2
)

replace github.com/xanzy/go-gitlab v0.39.0 => github.com/firelizzard18/go-gitlab v0.39.1-0.20201109074133-d129a84a82d1
