package configure

import (
	"fmt"
	"io"
	"net/url"
	"regexp"
	"strings"

	"github.com/pelletier/go-toml"
)

var reSSH = regexp.MustCompile(`^(\w+)@([\.\-\w]+):(.*)\.git$`)

func (c *Config) Encode(w io.Writer) error {
	err := toml.NewEncoder(w).Encode(c)
	if err != nil {
		return fmt.Errorf("cannot encode config: %v", err)
	}
	return nil
}

func (c *Config) Decode(r io.Reader) error {
	err := toml.NewDecoder(r).Decode(c)
	if err != nil {
		return fmt.Errorf("cannot decode config: %v", err)
	}
	return nil
}

func (c *Config) Marshal() ([]byte, error) {
	b, err := toml.Marshal(c)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal config: %v", err)
	}
	return b, nil
}

func (c *Config) Unmarshal(b []byte) error {
	err := toml.Unmarshal(b, c)
	if err != nil {
		return fmt.Errorf("cannot unmarshal config: %v", err)
	}
	return nil
}

type Config struct {
	Server struct {
		Default string `toml:"default"`
	} `toml:"server"`

	Servers Servers `toml:"servers"`
}

type Servers []Server

type Server struct {
	Name            string          `toml:"name"`
	URL             string          `toml:"url"`
	SSH             string          `toml:"ssh"`
	CredentialStore CredentialStore `toml:"credential-store"`
}

func (ss Servers) Get(name string) *Server {
	for i, s := range ss {
		if s.Name == name {
			return &ss[i]
		}
	}
	return nil
}

func (ss *Servers) Add(name, url string) {
	*ss = append(*ss, Server{Name: name, URL: url})
}

func (ss *Servers) Remove(name string) *Server {
	for i, s := range *ss {
		if s.Name == name {
			*ss = append((*ss)[:i], (*ss)[i+1:]...)
			return &s
		}
	}
	return nil
}

func (s *Server) sshHost() string {
	if s.SSH != "" {
		return s.SSH
	}

	v, err := url.Parse(s.URL)
	if err != nil {
		return ""
	}
	return v.Host
}

func (s *Server) Rel(urlStr string) (string, bool) {
	if u, err := url.Parse(urlStr); err == nil {
		switch u.Scheme {
		case "ssh":
			if u.Host != s.sshHost() {
				return "", false
			}
		default:
			v, err := url.Parse(s.URL)
			if err != nil {
				return "", false
			} else if u.Scheme != v.Scheme || u.Host != v.Host {
				return "", false
			}
		}

		path := u.Path
		if strings.HasPrefix(path, "/") {
			path = path[1:]
		}
		if strings.HasSuffix(path, ".git") {
			path = path[:len(path)-4]
		}
		return path, true
	}

	if m := reSSH.FindStringSubmatch(urlStr); m != nil {
		if m[2] != s.sshHost() {
			return "", false
		}
		return m[3], true
	}

	return "", false
}

type CredentialStore string

func (cs CredentialStore) IsAutomatic() bool {
	return cs == "" || cs == "auto" || cs == "automatic"
}

func (cs CredentialStore) TryNetRC() bool {
	return cs.IsAutomatic() || cs == "netrc"
}

func (cs CredentialStore) TryGnomeSS() bool {
	return cs.IsAutomatic() || cs == "gnome-secret-service"
}

func (cs CredentialStore) TryKeychain() bool {
	return cs.IsAutomatic() || cs == "keychain"
}

func (cs CredentialStore) TryWincred() bool {
	return cs.IsAutomatic() || cs == "wincred"
}
