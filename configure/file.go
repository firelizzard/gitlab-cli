package configure

import (
	"errors"
	"fmt"
	"os"
)

func NewFile(path string) (*FileConfig, error) {
	_, err := os.Stat(path)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, err
	}

	return &FileConfig{path: path}, nil
}

func LoadFile(path string) (*FileConfig, error) {
	fc := &FileConfig{path: path}

	if err := fc.Load(); err != nil {
		return nil, err
	}
	return fc, nil
}

func SaveFile(path string, config *Config) error {
	return (&FileConfig{Config: *config, path: path}).Save()
}

type FileConfig struct {
	Config

	path string
}

func (fc *FileConfig) Path() string { return fc.path }

func (fc *FileConfig) Load() error {
	f, err := os.Open(fc.path)
	if err != nil {
		return fmt.Errorf("cannot open config file: %w", err)
	}
	defer f.Close()

	return fc.Decode(f)
}

func (fc *FileConfig) Save() error {
	// marshal first, to avoid clobbering the file
	b, err := fc.Marshal()
	if err != nil {
		return err
	}

	f, err := os.Create(fc.path)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(b)
	return err
}
