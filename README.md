# *Unofficial* GitLab CLI

This is a simple command line tool for working with GitLab.

## Install

- [Browse latest `master` artifacts](https://gitlab.com/firelizzard/gitlab-cli/-/jobs/artifacts/master/browse?job=release)
- [Browse latest `v0.0.1` artifacts](https://gitlab.com/firelizzard/gitlab-cli/-/jobs/artifacts/v0.0.1/browse?job=release)
- `go install gitlab.com/firelizzard/gitlab-cli/cmd/gl`
- `git clone https://gitlab.com/firelizzard/gitlab-cli && cd gitlab-cli && go install ./cmd/gl`

## Setup

1. `gl server add gitlab-com gitlab.com`
2. Add a personal access token (choose one)
   - `gl server set-token netrc gitlab-com` ([netrc](https://ec.haxx.se/usingcurl/usingcurl-netrc))
   - `gl server set-token keychain gitlab-com` (macOS keychain, macOS-only)
   - `gl server set-token gnome gitlab-com` (Gnome Secret Service, Linux-only)
   - `gl server set-token wincred gitlab-com` (Windows Credential Manager, Windows-only)

`gl` can be used within a GitLab CI job without configuration by passing the
`--ci` flag, which instructs `gl` to pull configuration data from the standard
CI environment variables. This uses the CI job token for authentication, and
thus subcommands that rely on API calls that cannot be done with a CI token will
fail.

## Commands

See [Commands.md](./Commands.md) for a complete list of subcommands and usage.

* `gl clone <path>` - clone a GitLab project
* `gl checkout <id>` - checkout a branch by merge request ID
* `gl open [<ref>]` - open a GitLab reference in your browser
* `gl ci ...` - inspect GitLab CI pipelines, jobs, or artifacts, or download artifacts