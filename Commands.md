# Commands

### Global Flags

* `--config-file <path>` overrides the configuration file path (defaults to `~/.gitlab-cli.toml`).
* `--server <name>` sets the name of the server (which must exist in the configuration).
* `--ci` instructs `gl` to use GitLab CI variables for server, project, and
  pipeline selection and server authentication, unless overridden by other
  flags. Thus, `gl --ci ...` should work in a GitLab CI script with zero
  configuration. Note: `docker build ...` does not preseve enviromnent
  variables.

### Aliases

* `mr` or `mrs` for `merge-requests`
* `pipeline` or `pl` for `pipelines`
* `job` for `jobs`
* `dl` for `download`

## Git Operations

#### `gl clone <path>`

Clone a GitLab project.

#### `gl checkout <mr-id>`

Checkout the source branch of a merge request.

## Query

#### `gl open [<ref>]`

Open a GitLab reference, using GitLab Flavored Markdown syntax. Issues `#`,
epics `&`, and merge requests `!` are supported.

A call such as `gl open my/project!123` will resolve the path to a project
(issues, MRs) or group (epics) and open the appropriate path. A call such as `gl
open !123` assumes the reference is relative to the current repository's project
and thus will not work outside of a repository.

Calling `gl open` (with no argument) within a repository will open that
repository's GitLab project. Calling `gl open` (with no argument) outside of a
repository will open the GitLab server's home page.

`gl open [<ref>]` executes an OS-specific shell command:

  * macOS: `open "<url>"`
  * Linux: `xdg-open "<url>"`
  * Windows: `start "" "<url>"`

#### `gl all projects`

List all of the projects visible to you.

#### `gl my projects`

List all of the projects in your personal namespace.

#### `gl my merge-requests [all|authored|assigned]`

List merge requests authored by and/or assigned to you.

#### `gl merge-requests` within a repository

Resolve the repository remote to a GitLab project and list the merge requests of
that project.

#### `gl merge-requests <project-path>`

List the merge requests of the specified project.

## CI

CI subcommands must either specify a project with `--project <path>` or must be
run within a repository. `--project <path>` takes precedence over the current
repository's project. `--ref <name>` can be used to reduce scope, when
applicable. For subcommands other than `pipelines`, `--pipeline <id>|latest` can
be used to query a specific pipeline.

CI subcommands run within a repository will default to querying against the
current branch. To override this, pass `--ref '*'`.

#### `gl ci pipelines [<id>|latest]`

List pipelines, show details of a specific pipeline, or show details of the
latest pipeline.

#### `gl ci jobs [<id>|<name>]`

List jobs - optionally filtered by name, or show details of a specific job.

#### `gl ci artifacts <job_id>|<job_name>`

List artifacts of a job. If a job name is passed, `--pipeline <id>|latest` must
be specified, and the pipeline and job name must resolve to exactly one job.

#### `gl ci download <job_id>|<job_name> [<file>]`

Download the artifacts of a job as a ZIP, or extract a specific file from the
archive. `--output <path>` overrides the default file name and path. If a job
name is passed, `--pipeline <id>|latest` must be specified, and the pipeline and
job name must resolve to exactly one job.

## Configure

#### `gl server`

List configured GitLab instances.

#### `gl server add <name> <url>`

Add a GitLab instance.

#### `gl server remove <name>`

Remove a GitLab instance.

#### `gl server set-url <name> <url>`

Update the URL of a GitLab instance.

#### `gl server set-token netrc <name> [<token>]`

Store your GitLab access token in your netrc file. Prompts you to type in the
token if it is not passed as an arugment.

#### `gl server set-token keychain <name> [<token>]` (macOS-only)

Store your GitLab access token in the macOS KeyChain. Prompts you to type in the
token if it is not passed as an arugment.

#### `gl server set-token gnome <name> [<token>]` (Linux-only)

Store your GitLab access token in the Gnome (Linux) Secret Service. Prompts you
to type in the token if it is not passed as an arugment.

#### `gl server set-token wincred <name> [<token>]` (Windows-only)

Store your GitLab access token in the Windows Credential Manager. Prompts you to
type in the token if it is not passed as an arugment.

#### `git config gitlab-cli.server <name>` within a repository

Tell `gl` to use the specified server when resolving the Gitlab project for the
current repository.

#### `git config gitlab-cli.project <project-path>` within a repository

Tell `gl` to use the specified project path when resolving the Gitlab project
for the current repository.